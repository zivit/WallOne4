import QtQuick 2.5
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
import "style/"

Item {
    width: 480
    height: 300
    visible: hidingMenu.running ? true : secondBackground.opacity > 0
    z: 4
    state: "hidden"

    onVisibleChanged: {
        if (visible === false) {
            albumsForm.state = "start";
        }
    }

    onStateChanged: {
        if (state === "showed") {
            albumsForm.state = "show";
        }
        else if (state === "hidden") {
            albumsForm.state = "hide";
        }
    }

    readonly property int animationTime: 300

    Rectangle {
        id: secondBackground
        width: 480
        height: 300
        color: "#cc000000"
        z: 5

        MouseArea {
            id: secondBackgroundMouseArea
            anchors.fill: parent

            onClicked: albumsMenu.state = "hidden"
        }
    }

    AlbumsForm {
        id: albumsForm
    }

    states: [
        State {
            name: "showed"
            PropertyChanges { target: secondBackground; opacity: 1.0 }
        },
        State {
            name: "hidden"
            PropertyChanges { target: secondBackground; opacity: 0.0 }
        }
    ]

    transitions: [
        Transition {
            id: showingMenu
            from: "showed"
            to: "hidden"
            OpacityAnimator { target: secondBackground; duration: animationTime }
        },
        Transition {
            id: hidingMenu
            from: "hidden"
            to: "showed"
            OpacityAnimator { target: secondBackground; duration: animationTime }
        }
    ]
}
