import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "style/"

Item {    
    width: 160
    height: 120

    property alias aboutButton: aboutButton
    property alias feedbackButton: feedbackButton
    property alias settingsButton: settingsButton

    Button {
        id: settingsButton
        height: 40
        text: qsTr("Settings")
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        checked: false
        checkable: true
        style: MenuButtonStyle {}

        onCheckedChanged: {
            if (settingsButton.checked) {
                if (settingsForm.state != "show") settingsForm.state = "show"
            } else {
                settingsForm.state = "hide"
            }
        }

        onClicked: {
            if (feedbackButton.checked) feedbackButton.checked = false
            if (aboutButton.checked) aboutButton.checked = false
        }
    }

    Button {
        id: feedbackButton
        height: 40
        text: qsTr("Feedback")
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.topMargin: 0
        checked: false
        checkable: true
        anchors.top: settingsButton.bottom
        anchors.rightMargin: 0
        anchors.right: parent.right
        style: MenuButtonStyle2 {}

        onCheckedChanged: {
            if (feedbackButton.checked) {
                if (feedbackForm.state != "show") feedbackForm.state = "show"
            } else {
                feedbackForm.state = "hide"
            }
        }

        onClicked: {
            if (settingsButton.checked) settingsButton.checked = false
            if (aboutButton.checked) aboutButton.checked = false
        }
    }

    Button {
        id: aboutButton
        height: 40
        text: qsTr("About")
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.topMargin: 0
        checked: false
        checkable: true
        anchors.top: feedbackButton.bottom
        anchors.rightMargin: 0
        anchors.right: parent.right
        style: MenuButtonStyle {}

        onCheckedChanged: {
            if (aboutButton.checked) {
                if (aboutForm.state != "show") aboutForm.state = "show"
            } else {
                aboutForm.state = "hide"
            }
        }

        onClicked: {
            if (settingsButton.checked) settingsButton.checked = false
            if (feedbackButton.checked) feedbackButton.checked = false
        }
    }
}
