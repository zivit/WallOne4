import QtQuick 2.5
import QtQuick.Controls.Styles 1.4

ButtonStyle {
    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 25
        radius: control.width / 2

        color: control.pressed ? "#CCCCCC" : control.hovered ? "#E5E5E5" : "#FFFFFF"

        Behavior on color {
            ColorAnimation {
                duration: 100
            }
        }
    }
}
