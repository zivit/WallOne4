import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

ButtonStyle {
    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 25

        color: control.pressed ? "#dd000000" : control.hovered ? "#aa000000" : "#cc000000"

        Behavior on color {
            ColorAnimation {
                duration: 100
            }
        }
    }

    label: Component {
        Item {

            Image {
                anchors.centerIn: parent
                source: control.iconSource
            }
        }
    }

}
