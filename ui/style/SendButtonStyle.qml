import QtQuick 2.5
import QtQuick.Controls.Styles 1.4

ButtonStyle {
    background: Rectangle {
        color: control.pressed ? "#3A85AC" : control.hovered ? "#4395C0" : "#59A1C7"

        Behavior on color {
            ColorAnimation {
                duration: 100
            }
        }

        Image {
            anchors.centerIn: parent
        }
    }
}
