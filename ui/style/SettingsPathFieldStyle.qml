import QtQuick 2.5
import QtQuick.Controls.Styles 1.4

TextFieldStyle {
    textColor: "#3F3F3F"
    placeholderTextColor: "#A6A6A6"
    padding.left: 10
    renderType: Text.NativeRendering

    background: Rectangle {
        color: "white"
    }
}
