import QtQuick 2.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 1.4

ButtonStyle {
    background: Rectangle {
        implicitHeight: 40

        color: control.pressed ? "#D9D9D9" : control.hovered ? "#D9D9D9" : "white"

        Rectangle {
            anchors.left: parent.left
            anchors.leftMargin: 1
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: 3
            color: control.checked ? "#62B0D9" : parent.color
        }

        Behavior on color {
            ColorAnimation {
                duration: 100
            }
        }
    }

    label: Rectangle {
        anchors.fill: parent
        color: "#00000000"

        Text {
            text: control.text
            font.family: "Open Sans"
            anchors.left: parent.left
            anchors.leftMargin: 13
            anchors.top: parent.top
            anchors.topMargin: 8
            font.pixelSize: 11
            font.bold: true
            font.capitalization: Font.AllUppercase
            color: "#3F3F3F"
            smooth: true
            renderType: Text.NativeRendering
        }

        Image {
            anchors.centerIn: parent
            source: control.iconSource
        }
    }
}
