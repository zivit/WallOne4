import QtQuick 2.5
import QtQuick.Controls.Styles 1.4

ButtonStyle {
    background: Rectangle {
        color: control.pressed ? "#D9D9D9" : control.hovered ? "#E5E5E5" : "#F1F1F1"

        Behavior on color {
            ColorAnimation {
                duration: 100
            }
        }

        Image {
            anchors.centerIn: parent
        }
    }
}
