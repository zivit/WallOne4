import QtQuick 2.5
import QtQuick.Controls.Styles 1.4

TextAreaStyle {
    textColor: "#3F3F3F"
    textMargin: 10
    renderType: Text.NativeRendering
    backgroundColor: "white"
}
