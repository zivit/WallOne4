import QtQuick 2.5
import QtQuick.Controls.Styles 1.4

ButtonStyle {
    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 25

        color: control.pressed ? "#858585" : control.hovered ? "#959595" : "#A6A6A6"

        Behavior on color {
            ColorAnimation {
                duration: 100
            }
        }
    }
}
