import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

CheckBoxStyle {
    background: Rectangle {
        color: "white"
        implicitWidth: 31
        implicitHeight: 31
    }

    indicator: Rectangle {
        implicitWidth: 31
        implicitHeight: 31

        Image {
            id: checkImage
            source: "qrc:/images/resources/check.png"
            anchors.centerIn: parent
            state: control.checked ? "checked" : "unchecked"

            readonly property int animationTime: 300

            states: [
                State {
                    name: "checked"
                    PropertyChanges { target: checkImage; scale: 1 }
                    PropertyChanges { target: checkImage; opacity: 1 }
                },
                State {
                    name: "unchecked"
                    PropertyChanges { target: checkImage; scale: 0.5 }
                    PropertyChanges { target: checkImage; opacity: 0 }
                }
            ]

            transitions: [
                Transition {
                    from: "checked"
                    to: "unchecked"
                    ScaleAnimator { target: checkImage; duration: animationTime; easing.type: Easing.InCirc }
                    OpacityAnimator { target: checkImage; duration: animationTime; easing.type: Easing.InCirc }
                },
                Transition {
                    from: "unchecked"
                    to: "checked"
                    ScaleAnimator { target: checkImage; duration: animationTime; easing.type: Easing.OutCirc }
                    OpacityAnimator { target: checkImage; duration: animationTime; easing.type: Easing.OutCirc }
                }
            ]
        }
    }
}
