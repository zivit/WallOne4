import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

ComboBoxStyle {
    dropDownButtonWidth: 31
    textColor: "#3F3F3F"
    selectionColor: "white"
    selectedTextColor: "#3F3F3F"

    background: Rectangle {
        color: "white"

        Rectangle {
            color: control.pressed ? "#D9D9D9" : control.hovered ? "#E5E5E5" : "#F1F1F1"
            width: 31
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom

            Image {
                source: "qrc:/images/resources/combobox.png"
                anchors.centerIn: parent
            }

            Behavior on color {
                ColorAnimation {
                    duration: 100
                }
            }
        }
    }

    label: Text {
        text: control.textAt(control.currentIndex)
        font.family: "Open Sans"
        font.pixelSize: 10
        font.capitalization: Font.AllUppercase
        color: "#3F3F3F"
        renderType: Text.NativeRendering
        verticalAlignment: Text.AlignVCenter
        anchors.left: parent.left
        anchors.leftMargin: 5
    }    
}
