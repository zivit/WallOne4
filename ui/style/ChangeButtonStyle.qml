import QtQuick 2.5
import QtQuick.Controls.Styles 1.4

ButtonStyle {
    property int changeWidth: 100

    background: Rectangle {
        implicitHeight: 25
        color: control.pressed ? "#3A85AC" : control.hovered ? "#4395C0" : "#59A1C7"

        Behavior on color {
            ColorAnimation {
                duration: 100
            }
        }
    }

    label: Text {
        id: changeText
        text: control.text
        font.family: "Open Sans"
        font.pixelSize: 11
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.topMargin: -2
        color: "white"
        smooth: true
        font.capitalization: Font.AllUppercase
        renderType: Text.NativeRendering
    }
}
