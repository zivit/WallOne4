import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "style/"

Item {
    width: 320
    height: 300
    x: -width
    z: 6
    state: "start"

    readonly property int animationTime: 300

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }

    Rectangle {
        id: blueBackground
        color: "#62b0d9"
        anchors.fill: parent

        Rectangle {
            id: backgroundRectangle
            height: 130
            color: "#ffffff"
            clip: true
            anchors.right: parent.right
            anchors.rightMargin: 15
            anchors.top: parent.top
            anchors.topMargin: 15
            anchors.left: parent.left
            anchors.leftMargin: 15

            Text {
                id: albumText
                color: "#a6a6a6"
                text: qsTr("Album")
                renderType: Text.NativeRendering
                font.capitalization: Font.AllUppercase
                font.bold: true
                font.family: "Open Sans"
                font.pixelSize: 11
                anchors.left: parent.left
                anchors.leftMargin: 15
                anchors.top: parent.top
                anchors.topMargin: 11
            }

            Text {
                id: formatText
                color: "#a6a6a6"
                text: qsTr("Format")
                anchors.left: parent.left
                anchors.leftMargin: 15
                anchors.top: albumText.bottom
                anchors.topMargin: 8
                renderType: Text.NativeRendering
                font.capitalization: Font.AllUppercase
                font.bold: true
                font.family: "Open Sans"
                font.pixelSize: 11
            }

            Text {
                id: resolutionText
                color: "#a6a6a6"
                text: qsTr("Resolution")
                anchors.left: parent.left
                anchors.leftMargin: 15
                anchors.top: formatText.bottom
                anchors.topMargin: 8
                renderType: Text.NativeRendering
                font.capitalization: Font.AllUppercase
                font.bold: true
                font.family: "Open Sans"
                font.pixelSize: 11
            }

            Text {
                id: sizeText
                color: "#a6a6a6"
                text: qsTr("Size")
                anchors.left: parent.left
                anchors.leftMargin: 15
                anchors.top: resolutionText.bottom
                anchors.topMargin: 8
                renderType: Text.NativeRendering
                font.capitalization: Font.AllUppercase
                font.bold: true
                font.family: "Open Sans"
                font.pixelSize: 11
            }

            Text {
                id: licenseText
                color: "#a6a6a6"
                text: qsTr("License")
                anchors.left: parent.left
                anchors.leftMargin: 15
                anchors.top: sizeText.bottom
                anchors.topMargin: 8
                renderType: Text.NativeRendering
                font.capitalization: Font.AllUppercase
                font.bold: true
                font.family: "Open Sans"
                font.pixelSize: 11
            }

            Text {
                id: albumValue
                x: 246
                color: "#3f3f3f"
                text: qsTr("(none)")
                horizontalAlignment: Text.AlignRight
                anchors.right: parent.right
                anchors.rightMargin: 15
                anchors.top: albumText.top
                anchors.topMargin: 0
                renderType: Text.NativeRendering
                font.capitalization: Font.AllUppercase
                font.family: "Open Sans"
                font.pixelSize: 10
            }

            Text {
                id: formatValue
                x: 246
                color: "#3f3f3f"
                text: qsTr("(none)")
                horizontalAlignment: Text.AlignRight
                anchors.top: formatText.top
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 15
                renderType: Text.NativeRendering
                font.capitalization: Font.AllUppercase
                font.family: "Open Sans"
                font.pixelSize: 10
            }

            Text {
                id: resolutionValue
                x: 246
                color: "#3f3f3f"
                text: qsTr("(none)")
                horizontalAlignment: Text.AlignRight
                anchors.top: resolutionText.top
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 15
                renderType: Text.NativeRendering
                font.capitalization: Font.AllUppercase
                font.family: "Open Sans"
                font.pixelSize: 10
            }

            Text {
                id: sizeValue
                x: 246
                color: "#3f3f3f"
                text: qsTr("(none)")
                horizontalAlignment: Text.AlignRight
                anchors.top: sizeText.top
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 15
                renderType: Text.NativeRendering
                font.capitalization: Font.AllUppercase
                font.family: "Open Sans"
                font.pixelSize: 10
            }

            Text {
                id: licenseValue
                x: 246
                color: "#3f3f3f"
                text: qsTr("(none)")
                horizontalAlignment: Text.AlignRight
                anchors.top: licenseText.top
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 15
                renderType: Text.NativeRendering
                font.capitalization: Font.AllUppercase
                font.family: "Open Sans"
                font.pixelSize: 10
            }
        }

        Rectangle {
            id: shadowBox
            height: 2
            color: "#40407491"
            anchors.right: backgroundRectangle.right
            anchors.left: backgroundRectangle.left
            anchors.top: backgroundRectangle.bottom
        }
    }


    states: [
        State {
            name: "start"
            PropertyChanges { target: imageInfoForm; x: -width }
        },
        State {
            name: "show"
            PropertyChanges { target: imageInfoForm; x: 160 }
        },
        State {
            name: "hide"
            PropertyChanges { target: imageInfoForm; x: 480 }
        }
    ]

    transitions: [
        Transition {
            from: "start"
            to: "show"
            XAnimator { from: -160; duration: animationTime; easing.type: Easing.OutCirc }
        },
        Transition {
            id: hideTransition
            from: "show"
            to: "hide"
            XAnimator { duration: animationTime; easing.type: Easing.OutCirc }

            onRunningChanged: {
                if (!hideTransition.running) imageInfoForm.state = "start"
            }
        },
        Transition {
            from: "hide"
            to: "start"
            XAnimator { duration: 0 }
        }
    ]
}
