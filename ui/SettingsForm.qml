import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import Qt.labs.settings 1.0
import "style/"

Item {
    width: 320
    height: 300
    x: -width
    z: 6
    state: "start"

    readonly property int animationTime: 300

    Settings {
        property alias startupChecked: startupCheck.checked
        property alias updateChecked: updateCheck.checked
        property alias languageIndex: languageList.currentIndex
        property alias locationText: locationField.text
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }

    Rectangle {
        id: blueBackground
        color: "#62b0d9"
        visible: true
        anchors.fill: parent

        Text {
            id: systemText
            color: "#ffffff"
            text: qsTr("system")
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.top: parent.top
            anchors.topMargin: 15
            font.capitalization: Font.AllUppercase
            renderType: Text.NativeRendering
            font.bold: true
            font.family: "Open Sans"
            font.pixelSize: 11
        }

        Text {
            id: languageText
            color: "#ffffff"
            text: qsTr("language")
            visible: false
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.top: systemText.bottom
            anchors.topMargin: 20
            font.capitalization: Font.AllUppercase
            renderType: Text.NativeRendering
            font.family: "Open Sans"
            font.pixelSize: 10
        }

        Text {
            id: sturtupText
            color: "#ffffff"
            text: qsTr("launch on startup")
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.top: systemText.bottom
            anchors.topMargin: 21
            font.capitalization: Font.AllUppercase
            renderType: Text.NativeRendering
            font.family: "Open Sans"
            font.pixelSize: 10
        }

        Text {
            id: updateText
            color: "#ffffff"
            text: qsTr("auto update")
            anchors.top: sturtupText.bottom
            anchors.topMargin: 21
            anchors.left: parent.left
            anchors.leftMargin: 15
            font.capitalization: Font.AllUppercase
            renderType: Text.NativeRendering
            font.family: "Open Sans"
            font.pixelSize: 10
        }

        Text {
            id: backupText
            color: "#ffffff"
            text: qsTr("backup of albums")
            visible: false
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.top: updateText.bottom
            anchors.topMargin: 21
            font.capitalization: Font.AllUppercase
            renderType: Text.NativeRendering
            font.bold: true
            font.family: "Open Sans"
            font.pixelSize: 11
        }

        Text {
            id: locationText
            color: "#ffffff"
            text: qsTr("location")
            visible: false
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.top: backupText.bottom
            anchors.topMargin: 20
            font.capitalization: Font.AllUppercase
            renderType: Text.NativeRendering
            font.family: "Open Sans"
            font.pixelSize: 10
        }

        Rectangle {
            id: systemLine
            color: "#ffffff"
            anchors.top: parent.top
            anchors.topMargin: 22
            anchors.bottom: systemText.verticalCenter
            anchors.bottomMargin: 0
            anchors.left: systemText.right
            anchors.leftMargin: 15
            anchors.right: parent.right
            anchors.rightMargin: 15
        }

        Rectangle {
            id: backupLine
            y: 162
            height: 1
            color: "#ffffff"
            visible: false
            anchors.bottom: backupText.verticalCenter
            anchors.bottomMargin: 0
            anchors.left: backupText.right
            anchors.leftMargin: 15
            anchors.right: parent.right
            anchors.rightMargin: 15
        }

        ComboBox {
            id: languageList
            height: 31
            visible: false
            anchors.right: parent.right
            anchors.rightMargin: 15
            anchors.top: parent.top
            anchors.topMargin: 42
            anchors.left: languageText.right
            anchors.leftMargin: 77

            style: SettingsComboBoxStyle {}

            model: ListModel {
                ListElement { text: "English" }
                ListElement { text: "Русский" }
                ListElement { text: "Українська" }
            }
        }

        CheckBox {
            id: startupCheck
            x: 274
            width: 31
            height: 31
            checked: true
            anchors.top: sturtupText.verticalCenter
            anchors.topMargin: -15
            anchors.right: parent.right
            anchors.rightMargin: 15

            style: SettingsCheckStyle {}

            onClicked: {
                if (checked) {
                    appCore.enableLaunchOnStartup();
                }
                else {
                    appCore.disableLaunchOnStartup();
                }
            }
        }

        CheckBox {
            id: updateCheck
            x: 274
            width: 31
            height: 31
            checked: true
            anchors.right: parent.right
            anchors.rightMargin: 15
            anchors.top: updateText.bottom
            anchors.topMargin: -22

            style: SettingsCheckStyle {}
        }

        TextField {
            id: locationField
            x: 145
            width: 129
            height: 31
            visible: false
            anchors.top: locationButton.top
            anchors.topMargin: 0
            anchors.right: locationButton.left
            anchors.rightMargin: 0
            readOnly: true
            font.pointSize: 8
            font.family: "Open Sans"
            font.capitalization: Font.AllUppercase
            placeholderText: qsTr("/path/to/backup")

            style: SettingsPathFieldStyle {}
        }

        Button {
            id: locationButton
            x: 274
            width: 31
            height: 31
            visible: false
            anchors.top: locationText.bottom
            anchors.topMargin: -22
            anchors.right: parent.right
            anchors.rightMargin: 15
            iconSource: "qrc:/images/resources/folder.png"

            style: SettingsPathButtonStyle {}
        }

        Rectangle {
            id: shadowComboBox
            height: 2
            color: "#403f7490"
            visible: false
            anchors.top: languageList.bottom
            anchors.right: languageList.right
            anchors.left: languageList.left
        }

        Rectangle {
            id: shadowCheckBox
            height: 2
            color: "#40407491"
            anchors.right: startupCheck.right
            anchors.left: startupCheck.left
            anchors.top: startupCheck.bottom
        }

        Rectangle {
            id: shadowCheckBox1
            height: 2
            color: "#40407491"
            anchors.right: updateCheck.right
            anchors.left: updateCheck.left
            anchors.top: updateCheck.bottom
        }

        Rectangle {
            id: shadowLocationField
            height: 2
            color: "#40407491"
            visible: false
            anchors.right: locationButton.right
            anchors.left: locationField.left
            anchors.top: locationField.bottom
        }
    }


    states: [
        State {
            name: "start"
            PropertyChanges { target: settingsForm; x: -width }
        },
        State {
            name: "show"
            PropertyChanges { target: settingsForm; x: 160 }
        },
        State {
            name: "hide"
            PropertyChanges { target: settingsForm; x: 480 }
        }
    ]

    transitions: [
        Transition {
            from: "start"
            to: "show"
            XAnimator { from: -160; duration: animationTime; easing.type: Easing.OutCirc }
        },
        Transition {
            id: hideTransition
            from: "show"
            to: "hide"
            XAnimator { duration: animationTime; easing.type: Easing.OutCirc }

            onRunningChanged: {
                if (!hideTransition.running) settingsForm.state = "start"
            }
        },
        Transition {
            from: "hide"
            to: "start"
            XAnimator { duration: 0 }
        }
    ]
}
