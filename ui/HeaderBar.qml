import QtQuick 2.5
import QtQuick.Controls 1.4
import "style"

Rectangle {
    height: 36
    anchors.rightMargin: 0
    anchors.leftMargin: 0
    anchors.topMargin: 0
    anchors.right: parent.right
    anchors.left: parent.left
    anchors.top: parent.top

    property alias currentAlbumName: currentAlbumName.text
    property point prevMouse: "1,1"

    MouseArea {
        anchors.top: applicationWindow.top
        anchors.left: applicationWindow.left
        width: applicationWindow.width
        height: parent.height

        onPressed: {
            prevMouse = Qt.point(mouseX + 4, mouseY + 4);
        }

        onPositionChanged: {
            if (pressed) {
                applicationWindow.x = appCore.cursorPos().x - prevMouse.x;
                applicationWindow.y = appCore.cursorPos().y - prevMouse.y;
            }
        }
    }

    Button {
        id: gamburgerButton
        iconSource: gamburgerMenu.visible || infoMenu.visible || albumsMenu.visible ?
                        "qrc:/images/resources/menu_back.png" :
                        "qrc:/images/resources/menu.png"
        width: 34
        height: 34
        smooth: false
        anchors.topMargin: 1
        anchors.leftMargin: 1
        anchors.top: parent.top
        anchors.left: parent.left
        style: GamburgerButtonStyle {}

        onClicked: {
            if (gamburgerMenu.state === "showed") {
                gamburgerMenu.state = "hidden";
            }
            else if (infoMenu.state === "showed") {
                infoMenu.state = "hidden";
            }
            else if (albumsMenu.state === "showed") {
                albumsMenu.state = "hidden";
            }
            else {
                gamburgerMenu.state = "showed";
            }
        }
    }

    Button {
        id: changeButton
        y: 8
        height: 20
        text: qsTr("Change")
        anchors.left: currentAlbumName.right
        anchors.leftMargin: 8
        style: ChangeButtonStyle {}

        onClicked: {
            appCore.updateAlbumList();

            if (albumsMenu.state == "hidden") {
                albumsMenu.state = "showed";
            }
            else {
                albumsMenu.state = "hidden";
            }

            if (gamburgerMenu.state === "showed") {
                gamburgerMenu.state = "hidden";
            }
            else if (infoMenu.state === "showed") {
                infoMenu.state = "hidden";
            }
        }
    }

    Button {
        id: closeButton
        width: 34
        height: 34
        smooth: false
        iconSource: "qrc:/images/resources/close.png"
        anchors.top: parent.top
        anchors.topMargin: 1
        anchors.right: parent.right
        anchors.rightMargin: 1
        style: GamburgerButtonStyle {}

        onClicked: {
            applicationWindow.hide();

            if (canUpdate) {
                appCore.updateProgram();
            }
        }
    }

    Text {
        id: currentAlbumName
        text: qsTr("Cars")
        font.capitalization: Font.AllUppercase
        anchors.horizontalCenterOffset: -40
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 8
        font.bold: true
        font.family: "Open Sans"
        font.pixelSize: 14
        renderType: Text.NativeRendering
    }
}
