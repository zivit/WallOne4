import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "style/"

Item {
    width: 320
    height: 300
    x: -width
    z: 6
    state: "start"

    readonly property int animationTime: 300

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }

    Rectangle {
        id: blueBackground
        color: "#62b0d9"
        anchors.fill: parent

        Rectangle {
            id: backgroundRectangle
            color: "#ffffff"
            clip: true
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 37
            anchors.right: parent.right
            anchors.rightMargin: 15
            anchors.top: parent.top
            anchors.topMargin: 15
            anchors.left: parent.left
            anchors.leftMargin: 15

            Image {
                id: adviceImage
                x: 28
                y: 28
                width: 40
                height: 40
                source: "qrc:/images/resources/council.png"
            }

            Image {
                id: bugImage
                x: 129
                y: 28
                width: 32
                height: 36
                source: "qrc:/images/resources/bug.png"
            }

            Image {
                id: loveImage
                x: 222
                y: 28
                width: 40
                height: 37
                fillMode: Image.Stretch
                source: "qrc:/images/resources/love.png"
            }

            Text {
                id: adviceText
                x: 22
                color: "#3f3f3f"
                text: qsTr("Advice")
                anchors.horizontalCenter: adviceImage.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: 96
                renderType: Text.NativeRendering
                font.capitalization: Font.AllUppercase
                font.family: "Open Sans"
                font.pixelSize: 10
            }

            Text {
                id: bugText
                x: 121
                color: "#3f3f3f"
                text: qsTr("Bug")
                anchors.horizontalCenter: bugImage.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: 96
                renderType: Text.NativeRendering
                font.capitalization: Font.AllUppercase
                font.family: "Open Sans"
                font.pixelSize: 10
            }

            Text {
                id: loveText
                x: 230
                color: "#3f3f3f"
                text: qsTr("Love")
                anchors.horizontalCenter: loveImage.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: 96
                renderType: Text.NativeRendering
                font.capitalization: Font.AllUppercase
                font.family: "Open Sans"
                font.pixelSize: 10
            }

            Rectangle {
                id: line1
                x: 96
                y: 28
                width: 1
                height: 138
                color: "#f1f1f1"
            }

            Rectangle {
                id: line2
                x: 193
                y: 28
                width: 1
                height: 138
                color: "#f1f1f1"
            }

            Button {
                id: adviceButton
                x: 0
                y: 194
                width: 96
                height: 54
                iconSource: "qrc:/images/resources/pencil.png"

                style: SettingsPathButtonStyle {}
            }

            Button {
                id: bugButton
                x: 97
                y: 194
                width: 96
                height: 54
                iconSource: "qrc:/images/resources/pencil.png"

                style: SettingsPathButtonStyle {}
            }

            Button {
                id: loveButton
                x: 194
                y: 194
                width: 96
                height: 54
                iconSource: "qrc:/images/resources/cup.png"

                style: SettingsPathButtonStyle {}
            }
        }

        Rectangle {
            id: shadowBox
            height: 2
            color: "#40407491"
            anchors.right: backgroundRectangle.right
            anchors.left: backgroundRectangle.left
            anchors.top: backgroundRectangle.bottom
        }

        Text {
            id: emailText
            x: 115
            y: 271
            color: "#ffffff"
            text: qsTr("wallone@gmail.com")
            font.capitalization: Font.AllUppercase
            renderType: Text.NativeRendering
            font.family: "Open Sans"
            anchors.bottomMargin: 12
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 10
        }
    }


    states: [
        State {
            name: "start"
            PropertyChanges { target: feedbackForm; x: -width }
        },
        State {
            name: "show"
            PropertyChanges { target: feedbackForm; x: 160 }
        },
        State {
            name: "hide"
            PropertyChanges { target: feedbackForm; x: 480 }
        }
    ]

    transitions: [
        Transition {
            from: "start"
            to: "show"
            XAnimator { from: -160; duration: animationTime; easing.type: Easing.OutCirc }
        },
        Transition {
            id: hideTransition
            from: "show"
            to: "hide"
            XAnimator { duration: animationTime; easing.type: Easing.OutCirc }

            onRunningChanged: {
                if (!hideTransition.running) feedbackForm.state = "start"
            }
        },
        Transition {
            from: "hide"
            to: "start"
            XAnimator { duration: 0 }
        }
    ]
}
