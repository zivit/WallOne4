import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import "style"
import QtQuick.Controls 2.1

Item {
    width: 480
    height: 300

    property alias albumModel: albumModel

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }

    Rectangle {
        id: rectangle
        color: "#62b0d9"
        clip: true
        anchors.fill: parent

        ListModel {
            id: albumModel
        }

        GridView {
            id: gridView
            width: 444
            anchors.left: parent.left
            anchors.leftMargin: 26
            anchors.top: parent.top
            anchors.topMargin: 8
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 8
            flow: GridView.FlowLeftToRight
            cellWidth: 214
            cellHeight: 137

            ScrollBar.vertical: ScrollBar {
                visible: gridView.count < 5 ? false : true

                contentItem: Rectangle {
                    implicitWidth: 3
                    implicitHeight: 25
                    color: parent.pressed ? "#F1F1F1" : "white"
                }
            }

            delegate: AlbumItem {}
            model: albumModel
        }
    }

}
