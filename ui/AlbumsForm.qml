import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "style/"

Item {
    width: 444
    height: 300
    x: -width
    z: 6
    state: "start"

    readonly property int animationTime: 300
    property var albumList: []

    Connections {
        target: appCore

        onAlbumListDownloaded: {
            albumList = albumStringList

            albumTable.albumModel.clear()

            for (var i = 0; i < albumList.length; ++i) {
                var link = albumList[i].split("/")[0]
                var author = albumList[i].split("/")[1]
                var wallpapersCount = albumList[i].split("/")[2];
                var albumName = albumList[i].split("/")[3] // TODO: Language index
                albumTable.albumModel.append({
                    previewSource: GLOBALS.WALLONE_DOMAIN + GLOBALS.LIST_DIR + "/" + link + "/preview.jpg",
                    authorNameText: author,
                    count: wallpapersCount,
                    albumNameText: albumName
                    })
            }
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }

    Rectangle {
        id: blueBackground
        color: "#62b0d9"
        anchors.fill: parent

        AlbumTable {
            id: albumTable
        }
    }


    states: [
        State {
            name: "start"
            PropertyChanges { target: albumsForm; x: -width }
        },
        State {
            name: "show"
            PropertyChanges { target: albumsForm; x: 0 }
        },
        State {
            name: "hide"
            PropertyChanges { target: albumsForm; x: 480 }
        }
    ]

    transitions: [
        Transition {
            from: "start"
            to: "show"
            XAnimator { from: -width; duration: animationTime; easing.type: Easing.OutCirc }
        },
        Transition {
            from: "show"
            to: "hide"
            XAnimator { duration: animationTime; easing.type: Easing.OutCirc }

            onRunningChanged: {
                if (!running) {
                    albumsForm.state = "start";
                }
            }
        },
        Transition {
            from: "hide"
            to: "start"
            XAnimator { duration: 0 }
        }
    ]
}
