import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "style/"

Item {
    width: 320
    height: 300
    x: -width
    z: 6
    state: "start"

    readonly property int animationTime: 300

    Rectangle {
        id: blueBackground
        color: "#62b0d9"
        anchors.fill: parent

        Rectangle {
            id: backgroundRectangle
            color: "#ffffff"
            clip: true
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 37
            anchors.right: parent.right
            anchors.rightMargin: 15
            anchors.top: parent.top
            anchors.topMargin: 15
            anchors.left: parent.left
            anchors.leftMargin: 15

            Image {
                id: logoImage
                x: 95
                y: 30
                width: 108
                height: 50
                anchors.horizontalCenter: parent.horizontalCenter
                source: "qrc:/images/resources/logo.png"
            }

            Text {
                id: appNameText
                x: 115
                y: 110
                color: "#3f3f3f"
                text: "WallOne"
                anchors.horizontalCenter: parent.horizontalCenter
                renderType: Text.NativeRendering
                font.bold: true
                font.family: "Open Sans"
                font.pixelSize: 14
            }

            Text {
                id: sloganText
                x: 94
                y: 131
                color: "#3f3f3f"
                text: qsTr("Wallpaper for a day")
                anchors.horizontalCenter: parent.horizontalCenter
                renderType: Text.NativeRendering
                font.capitalization: Font.AllUppercase
                font.family: "Open Sans"
                font.pixelSize: 10
            }

            Text {
                id: versionText
                x: 121
                y: 162
                color: "#3f3f3f"
                text: qsTr("Version") + " " + GLOBALS.WALLONE_VERSION
                font.bold: true
                anchors.horizontalCenter: parent.horizontalCenter
                renderType: Text.NativeRendering
                font.capitalization: Font.AllUppercase
                font.family: "Open Sans"
                font.pixelSize: 11
            }

            Rectangle {
                id: authorsPanel
                x: 0
                y: 194
                width: 290
                height: 54
                color: "#f1f1f1"

                Text {
                    id: developerText
                    color: "#a6a6a6"
                    text: qsTr("Developer")
                    anchors.left: parent.left
                    anchors.leftMargin: 15
                    anchors.top: parent.top
                    anchors.topMargin: 12
                    renderType: Text.NativeRendering
                    font.capitalization: Font.AllUppercase
                    font.family: "Open Sans"
                    font.pixelSize: 10
                }

                Text {
                    id: developerName
                    x: 244
                    color: "#a6a6a6"
                    text: qsTr("Vitaliy Zinchenko")
                    anchors.top: parent.top
                    anchors.topMargin: 12
                    anchors.right: parent.right
                    anchors.rightMargin: 15
                    font.capitalization: Font.AllUppercase
                    renderType: Text.NativeRendering
                    font.family: "Open Sans"
                    font.pixelSize: 10
                }

                Text {
                    id: designerText
                    y: 44
                    color: "#a6a6a6"
                    text: qsTr("GUI Designer")
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 12
                    anchors.left: parent.left
                    anchors.leftMargin: 15
                    font.capitalization: Font.AllUppercase
                    renderType: Text.NativeRendering
                    font.family: "Open Sans"
                    font.pixelSize: 10
                }

                Text {
                    id: designerName
                    x: 244
                    y: 44
                    color: "#a6a6a6"
                    text: qsTr("Dan Slomchinskiy")
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 12
                    anchors.right: parent.right
                    anchors.rightMargin: 15
                    font.capitalization: Font.AllUppercase
                    renderType: Text.NativeRendering
                    font.family: "Open Sans"
                    font.pixelSize: 10
                }
            }
        }

        Rectangle {
            id: shadowBox
            height: 2
            color: "#40407491"
            anchors.right: backgroundRectangle.right
            anchors.left: backgroundRectangle.left
            anchors.top: backgroundRectangle.bottom
        }

        Text {
            id: corporationText
            x: 115
            y: 271
            color: "#ffffff"
            text: qsTr("PopaBanana Software")
            font.capitalization: Font.AllUppercase
            renderType: Text.NativeRendering
            font.family: "Open Sans"
            anchors.bottomMargin: 12
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 10
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }

    states: [
        State {
            name: "start"
            PropertyChanges { target: aboutForm; x: -width }
        },
        State {
            name: "show"
            PropertyChanges { target: aboutForm; x: 160 }
        },
        State {
            name: "hide"
            PropertyChanges { target: aboutForm; x: 480 }
        }
    ]

    transitions: [
        Transition {
            from: "start"
            to: "show"
            XAnimator { from: -160; duration: animationTime; easing.type: Easing.OutCirc }
        },
        Transition {
            id: hideTransition
            from: "show"
            to: "hide"
            XAnimator { duration: animationTime; easing.type: Easing.OutCirc }

            onRunningChanged: {
                if (!hideTransition.running) aboutForm.state = "start"
            }
        },
        Transition {
            from: "hide"
            to: "start"
            XAnimator { duration: 0 }
        }
    ]
}
