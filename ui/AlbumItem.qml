import QtQuick 2.5
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.0
import "style"

Item {
    id: item1
    width: 214
    height: 137

    property bool mouseAreaHovered: false
    property int wallpapersCount: albumModel.count

    MouseArea {
        id: mouseArea
        anchors.rightMargin: 0
        anchors.leftMargin: 0
        anchors.bottomMargin: 0
        anchors.top: content.top
        anchors.right: content.right
        anchors.bottom: content.bottom
        anchors.left: content.left
        anchors.topMargin: 0
        hoverEnabled: true

        onEntered: {
            mouseAreaHovered = true;
        }

        onExited: {
            mouseAreaHovered = false;
        }
    }

    Rectangle {
        id: background
        color: "#62b0d9"
        anchors.fill: parent
    }


    Rectangle {
        id: content
        width: 199
        height: 120
        color: "#ffffff"
        anchors.left: parent.left
        anchors.leftMargin: 7
        anchors.top: parent.top
        anchors.topMargin: 7

        Image {
            id: previewImage
            height: 62
            anchors.top: parent.top
            anchors.topMargin: 1
            anchors.right: parent.right
            anchors.rightMargin: 1
            anchors.left: parent.left
            anchors.leftMargin: 1
            source: previewSource
            fillMode: Image.PreserveAspectCrop
        }

        Text {
            id: albumName
            x: 16
            y: 75
            color: "#3f3f3f"
            text: albumNameText
            font.capitalization: Font.AllUppercase
            font.bold: true
            font.family: "Open Sans"
            renderType: Text.NativeRendering
            font.pixelSize: 13
        }

        Text {
            id: authorName
            x: 16
            y: 94
            color: "#a6a6a6"
            font.family: "Open Sans"
            text: authorNameText
            font.capitalization: Font.AllUppercase
            renderType: Text.NativeRendering
            font.pixelSize: 10
        }

        Rectangle {
            id: backgroundButtons
            color: "#ffffff"
            opacity: pushPreview.hovered || mouseAreaHovered ? 1 : 0
            anchors.bottomMargin: 0
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: previewImage.bottom
            anchors.top: parent.top
            anchors.leftMargin: 0

            Behavior on opacity {
                OpacityAnimator { duration: 250 }
            }

            Button {
                id: pushPreview
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 1
                anchors.top: parent.top
                anchors.topMargin: 1
                iconSource: "qrc:/images/resources/play_album.png"
                anchors.right: parent.right
                anchors.rightMargin: 1
                anchors.left: parent.left
                anchors.leftMargin: 1

                style: SettingsPathButtonStyle {}

                onClicked: {
                    tresh.threshold = 0;
                    albumsMenu.state = "hidden";
                    var sourcePath = "" + previewImage.source;
                    var lastIndex = sourcePath.lastIndexOf("/");
                    sourcePath = sourcePath.slice(0, lastIndex);

                    mainSettings.wallpapersCount = wallpapersCount
                    mainSettings.wallpaperPath = sourcePath
                    mainSettings.savedDate = new Date

                    headerBar.currentAlbumName = albumName.text;
                    appCore.setWallpapersCount(wallpapersCount);
                    appCore.downloadWallpaper(sourcePath);
                }
            }
        }
    }

    Rectangle {
        id: shadow
        x: 7
        y: 127
        height: 2
        color: "#403f7490"
        anchors.left: content.left
        anchors.leftMargin: 0
        anchors.right: content.right
        anchors.rightMargin: 0
        anchors.top: content.bottom
        anchors.topMargin: 0
    }

}
