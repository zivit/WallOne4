import QtQuick 2.5
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
import "style/"

Item {
    width: 480
    height: 300
    visible: hidingMenu.running ? true : secondBackground.opacity > 0
    z: 4
    state: "hidden"

    readonly property int animationTime: 300

    onStateChanged: {
        if (infoMenu.state == "hidden") {
            infoMenuButtons.informationButton.checked = false;
            infoMenuButtons.complainButton.checked = false;
            infoMenuButtons.saveButton.checked = false;
        }
    }

    Rectangle {
        id: secondBackground
        width: 480
        height: 300
        color: "#cc000000"
        z: 5

        MouseArea {
            id: secondBackgroundMouseArea
            anchors.fill: parent

            onClicked: infoMenu.state = "hidden"
        }
    }

    Rectangle {
        id: settingsBackground
        color: "white"
        width: 160
        height: 300
        x: 0
        y: 0
        z: 8

        InfoMenuButtons { id: infoMenuButtons }
    }

    ImageInfoForm {
        id: imageInfoForm
    }

    ComplainForm {
        id: complainForm
    }

//    AboutMenu {
//        id: aboutMenu
//    }

    states: [
        State {
            name: "showed"
            PropertyChanges { target: settingsBackground; x: 0 }
            PropertyChanges { target: secondBackground; opacity: 1.0 }
        },
        State {
            name: "hidden"
            PropertyChanges { target: settingsBackground; x: -width }
            PropertyChanges { target: secondBackground; opacity: 0.0 }
        }
    ]

    transitions: [
        Transition {
            id: showingMenu
            from: "showed"
            to: "hidden"
            XAnimator { target: settingsBackground; duration: animationTime; easing.type: Easing.OutCirc }
            OpacityAnimator { target: secondBackground; duration: animationTime }
        },
        Transition {
            id: hidingMenu
            from: "hidden"
            to: "showed"
            XAnimator { target: settingsBackground; duration: animationTime; easing.type: Easing.OutCirc }
            OpacityAnimator { target: secondBackground; duration: animationTime }
        }
    ]
}
