import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "style/"

Item {
    width: 320
    height: 300
    x: -width
    z: 6
    state: "start"

    readonly property int animationTime: 300

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }

    Rectangle {
        id: blueBackground
        color: "#62b0d9"
        anchors.fill: parent

        Text {
            id: causeText
            y: 28
            text: qsTr("Cause")
            verticalAlignment: Text.AlignTop
            horizontalAlignment: Text.AlignLeft
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.verticalCenter: causesList.verticalCenter
            color: "white"
            font.family: "Open Sans"
            font.pixelSize: 10
            font.capitalization: Font.AllUppercase
            renderType: Text.NativeRendering
        }

        Text {
            id: emailText
            y: 82
            text: qsTr("Email")
            anchors.verticalCenter: emailField.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 15
            verticalAlignment: Text.AlignVCenter
            color: "white"
            font.family: "Open Sans"
            font.pixelSize: 10
            font.capitalization: Font.AllUppercase
            renderType: Text.NativeRendering
        }

        Text {
            id: messageText
            text: qsTr("Message")
            anchors.top: messageArea.top
            anchors.topMargin: 9
            anchors.left: parent.left
            anchors.leftMargin: 15
            verticalAlignment: Text.AlignTop
            horizontalAlignment: Text.AlignHCenter
            color: "white"
            font.family: "Open Sans"
            font.pixelSize: 10
            font.capitalization: Font.AllUppercase
            renderType: Text.NativeRendering
        }

        ComboBox {
            id: causesList
            x: 157
            width: 160
            height: 31
            anchors.top: parent.top
            anchors.topMargin: 15
            anchors.right: parent.right
            anchors.rightMargin: 15

            style: SettingsComboBoxStyle {}

            model: ListModel {
                ListElement { text: qsTr("Spam") }
                ListElement { text: qsTr("Violates copyrights") }
                ListElement { text: qsTr("Extremism") }
                ListElement { text: qsTr("Porn") }
            }
        }

        TextField {
            id: emailField
            x: 145
            width: 160
            height: 31
            anchors.right: parent.right
            anchors.rightMargin: 15
            anchors.top: causesList.bottom
            anchors.topMargin: 6
            placeholderText: qsTr("Enter your email")
            font.family: "Open Sans"
            font.pixelSize: 10
            font.capitalization: Font.AllUppercase

            style: SettingsPathFieldStyle {}
        }

        TextArea {
            id: messageArea
            x: 145
            width: 160
            anchors.bottom: sendButton.top
            anchors.bottomMargin: 6
            frameVisible: false
            textColor: "#3f3f3f"
            font.capitalization: Font.AllUppercase
            font.pointSize: 8
            font.family: "Open Sans"
            anchors.right: parent.right
            anchors.rightMargin: 15
            anchors.top: emailField.bottom
            anchors.topMargin: 6

            style: ComplainAreaStyle {}
        }

        Button {
            id: sendButton
            x: 225
            y: 255
            width: 31
            height: 31
            anchors.right: parent.right
            anchors.rightMargin: 15
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 15
            iconSource: "qrc:/images/resources/send.png"

            style: SendButtonStyle {}
        }

        Rectangle {
            id: shadowComboBox
            height: 2
            color: "#403f7490"
            anchors.rightMargin: 0
            anchors.leftMargin: 0
            anchors.topMargin: 0
            anchors.top: causesList.bottom
            anchors.left: causesList.left
            anchors.right: causesList.right
        }

        Rectangle {
            id: shadowComboBox1
            x: -6
            y: 5
            height: 2
            color: "#403f7490"
            anchors.rightMargin: 0
            anchors.top: emailField.bottom
            anchors.left: emailField.left
            anchors.leftMargin: 0
            anchors.right: emailField.right
            anchors.topMargin: 0
        }

        Rectangle {
            id: shadowComboBox2
            height: 2
            color: "#403f7490"
            anchors.rightMargin: 0
            anchors.top: messageArea.bottom
            anchors.left: messageArea.left
            anchors.leftMargin: 0
            anchors.right: messageArea.right
            anchors.topMargin: 0
        }
    }


    states: [
        State {
            name: "start"
            PropertyChanges { target: complainForm; x: -width }
        },
        State {
            name: "show"
            PropertyChanges { target: complainForm; x: 160 }
        },
        State {
            name: "hide"
            PropertyChanges { target: complainForm; x: 480 }
        }
    ]

    transitions: [
        Transition {
            from: "start"
            to: "show"
            XAnimator { from: -160; duration: animationTime; easing.type: Easing.OutCirc }
        },
        Transition {
            id: hideTransition
            from: "show"
            to: "hide"
            XAnimator { duration: animationTime; easing.type: Easing.OutCirc }

            onRunningChanged: {
                if (!hideTransition.running) complainForm.state = "start"
            }
        },
        Transition {
            from: "hide"
            to: "start"
            XAnimator { duration: 0 }
        }
    ]
}
