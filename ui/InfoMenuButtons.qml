import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "style/"

Item {
    width: 160
    height: 120

    property alias informationButton: informationButton
    property alias complainButton: complainButton
    property alias saveButton: saveButton

    Button {
        id: informationButton
        height: 40
        text: qsTr("Information")
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        checked: false
        checkable: true
        style: MenuButtonStyle {}

        onCheckedChanged: {
            if (informationButton.checked) {
                if (imageInfoForm.state != "show") imageInfoForm.state = "show"
            } else {
                imageInfoForm.state = "hide"
            }
        }

        onClicked: {
            if (complainButton.checked) complainButton.checked = false
            if (saveButton.checked) saveButton.checked = false
        }
    }

    Button {
        id: complainButton
        height: 40
        text: qsTr("Complain")
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.topMargin: 0
        checked: false
        checkable: true
        anchors.top: informationButton.bottom
        anchors.rightMargin: 0
        anchors.right: parent.right
        style: MenuButtonStyle2 {}

        onCheckedChanged: {
            if (complainButton.checked) {
                if (complainForm.state != "show") complainForm.state = "show"
            } else {
                complainForm.state = "hide"
            }
        }

        onClicked: {
            if (informationButton.checked) informationButton.checked = false
            if (saveButton.checked) saveButton.checked = false
        }
    }

    Button {
        id: saveButton
        height: 40
        text: qsTr("Save to PC")
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.topMargin: 0
        checked: false
        checkable: true
        anchors.top: complainButton.bottom
        anchors.rightMargin: 0
        anchors.right: parent.right
        style: MenuButtonStyle {}

        onCheckedChanged: {
            if (saveButton.checked) {
//                if (aboutMenu.state != "show") aboutMenu.state = "show"
            } else {
//                aboutMenu.state = "hide"
            }
        }

        onClicked: {
            if (informationButton.checked) informationButton.checked = false
            if (complainButton.checked) complainButton.checked = false
        }
    }
}
