#pragma once

#include <QObject>
#include <QString>

class Globals : public QObject
{
    Q_OBJECT
public:
    Globals() {}
    virtual ~Globals() {}

    static const QString WALLONE_VERSION;
    Q_PROPERTY(const QString WALLONE_VERSION MEMBER WALLONE_VERSION CONSTANT)

    static const QString WALLONE_VERSION_URL;
    Q_PROPERTY(const QString WALLONE_VERSION_URL MEMBER WALLONE_VERSION_URL CONSTANT)

    static const QString WALLONE_UPDATE_URL;
    Q_PROPERTY(const QString WALLONE_UPDATE_URL MEMBER WALLONE_UPDATE_URL CONSTANT)

    static const QString WALLONE_DOMAIN;
    Q_PROPERTY(const QString WALLONE_DOMAIN MEMBER WALLONE_DOMAIN CONSTANT)

    static const QString LIST_DIR;
    Q_PROPERTY(const QString LIST_DIR MEMBER LIST_DIR CONSTANT)

    static const QString UPDATES_DIR;
    Q_PROPERTY(const QString UPDATES_DIR MEMBER UPDATES_DIR CONSTANT)
};
