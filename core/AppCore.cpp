#include "AppCore.hpp"

//------------------------------------------------------------------------------
AppCore::AppCore(QObject *parent) : QObject(parent)
{
    auto trayIconMenu = new QMenu();

    auto openWindow = new QAction(tr("Open WallOne"), this);
    auto minimizeWindow = new QAction(tr("Minimize to tray"), this);
    auto quitAction = new QAction(tr("Quit"), this);

    connect(openWindow, &QAction::triggered, [=]() {
        emit trayShowTriggered();
    });
    connect(minimizeWindow, &QAction::triggered, [=]() {
        emit trayMinimizeTriggered();
    });
    connect(quitAction, &QAction::triggered, [=]() {
        emit trayQuitTriggered();
    });

    trayIconMenu->addAction(openWindow);
    trayIconMenu->addAction(minimizeWindow);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(QIcon(":/images/resources/wallone.ico"), this);
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->show();

    updateAlbumList();
    checkUpdates();
}
//------------------------------------------------------------------------------
void AppCore::updateAlbumList()
{
    auto albumListDownloader = new HttpDownloader(this);
    connect(albumListDownloader, &HttpDownloader::fileDownloaded,
            this, &AppCore::parseAlbumList);

    QDir().mkpath(qApp->applicationDirPath() + Globals::LIST_DIR);
    albumListDownloader->download(QUrl(Globals::WALLONE_DOMAIN + Globals::LIST_DIR + "/list.txt"), QDir(qApp->applicationDirPath() + Globals::LIST_DIR));
}
//------------------------------------------------------------------------------
void AppCore::checkUpdates()
{
    auto verisonDownloader = new HttpDownloader(this);
    connect(verisonDownloader, &HttpDownloader::fileDownloaded,
            this, &AppCore::saveVersionFile);

    verisonDownloader->download(Globals::WALLONE_VERSION_URL, qApp->applicationDirPath());
}
//------------------------------------------------------------------------------
void AppCore::updateProgram()
{
    QString fileExt;
#ifdef Q_OS_WIN
    fileExt = ".exe";
#endif //Q_OS_WIN

#ifdef Q_OS_MAC
    fileExt = ".app";
#endif //Q_OS_WIN

    qDebug() << "Run updater";
    QProcess runUpdater;
    runUpdater.startDetached(qApp->applicationDirPath() + "/Updater" + fileExt);

    qApp->quit();
}
//------------------------------------------------------------------------------
void AppCore::saveVersionFile(QDir dir)
{
    QTimer t;
    t.singleShot(200, [this, dir]() {
        QFile versionFile(dir.path());

        if (versionFile.open(QIODevice::ReadOnly)) {
            QTextStream versionStream(&versionFile);
            QString version;
            version = versionStream.readLine();

            if (version != Globals::WALLONE_VERSION) {
                downloadUpdate();
                qDebug() << "Update available!";
            }
            else {
                qDebug() << "Latest version!";
            }
        }
        versionFile.close();
    });
}
//------------------------------------------------------------------------------
void AppCore::downloadUpdate()
{
    auto updateDownloader = new HttpDownloader(this);
    connect(updateDownloader, &HttpDownloader::fileDownloaded, [=]() {
        qDebug() << "Have update";
        emit haveUpdate();
    });

    QDir dir(qApp->applicationDirPath() + "/wallone/updates");
    dir.mkpath(qApp->applicationDirPath() + "/wallone/updates");

    qDebug() << dir.path();

    updateDownloader->download(Globals::WALLONE_UPDATE_URL, dir);
}
//------------------------------------------------------------------------------
void AppCore::setWallpapersCount(int count)
{
    wallpapersCount = count;
}
//------------------------------------------------------------------------------
void AppCore::downloadWallpaper(QUrl url)
{
    auto wallpaperDownloader = new HttpDownloader(this);
    connect(wallpaperDownloader, &HttpDownloader::fileDownloaded, [=](QDir dir) {        
        QString path = dir.path();
#ifdef Q_OS_WIN
        path.insert(1, ":");
#endif //Q_OS_WIN
        emit wallpaperDownloaded(QUrl(path));
    });

    QDir dir(qApp->applicationDirPath() + url.path());
    dir.mkpath(dir.path());

    url.setUrl(url.url() + "/" + QString("%1").arg(getWallpaperIndex(dir)) + ".jpg");
    wallpaperDownloader->download(url, dir);
}
//------------------------------------------------------------------------------
int AppCore::getWallpaperIndex(QDir dir)
{
    QFile counterFile(dir.path() + "/counter.txt");
    bool isCounterFileExist = counterFile.exists() ? true : false;
    int wallpaperIndex = 0;

    if (counterFile.open(QIODevice::ReadOnly)) {
        QTextStream counterStream(&counterFile);
        if (isCounterFileExist) {
            counterStream >> wallpaperIndex;
        }
    }
    counterFile.close();

    QSettings settings;
    QDate date = settings.value("savedDate", QDate(2000, 1, 1)).toDate();

    if (date.toString() != QDate::currentDate().toString()) {
        ++wallpaperIndex;
    }

    if (counterFile.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        QTextStream counterStream(&counterFile);
        if (isCounterFileExist) {
            if (wallpaperIndex > wallpapersCount) {
                counterStream << 1;
                wallpaperIndex = 1;
            }
            else {
                counterStream << wallpaperIndex;
            }
        }
        else {
            wallpaperIndex = 1;
            counterStream << wallpaperIndex;
        }
    }

    return wallpaperIndex;
}
//------------------------------------------------------------------------------
void AppCore::setWallpaper(QUrl url)
{
    QTimer t;
    t.singleShot(200, [this, url]() {
        auto wallpaperManager = new WallpaperManager(this);
        wallpaperManager->setWallpaper(QDir(url.path()));
    });

}
//------------------------------------------------------------------------------
QPointF AppCore::cursorPos()
{
    return QCursor::pos();
}
//------------------------------------------------------------------------------
void AppCore::enableLaunchOnStartup()
{
#ifdef Q_OS_LINUX
    enableLaunchOnStartupLinux();
#endif //Q_OS_LINUX

#ifdef Q_OS_WIN
    enableLaunchOnStartupWindows();
#endif //Q_OS_WIN
}
//------------------------------------------------------------------------------
void AppCore::enableLaunchOnStartupLinux()
{
    QString autostartPath = QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).at(0) + QLatin1String("/autostart");
    QDir autorunDir(autostartPath);

    if(!autorunDir.exists()){
        autorunDir.mkpath(autostartPath);
    }

    QFile autorunFile(autostartPath + QLatin1String("/WallOne.desktop"));

    if (!autorunFile.exists()){
        if (autorunFile.open(QFile::WriteOnly)){
            QString autorunContent("[Desktop Entry]\n"
                                   "Type=Application\n"
                                   "Exec=" + qApp->applicationFilePath() +
                                   " -hide\n"
                                   "Icon=" + qApp->applicationDirPath() +
                                   "/WallOneLogo.png\n"
                                   "Hidden=false\n"
                                   "NoDisplay=false\n"
                                   "X-GNOME-Autostart-enabled=true\n"
                                   "Name=WallOne\n"
                                   "Comment=WallOne\n");
            QTextStream outStream(&autorunFile);
            outStream << autorunContent;
            autorunFile.setPermissions(QFileDevice::ExeUser|QFileDevice::ExeOwner|QFileDevice::ExeOther|QFileDevice::ExeGroup|
                                       QFileDevice::WriteUser|QFileDevice::ReadUser);
            autorunFile.close();
        }
    }
}
//------------------------------------------------------------------------------
void AppCore::enableLaunchOnStartupWindows()
{
    QSettings settings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);
    settings.setValue(qApp->applicationName(), QDir::toNativeSeparators(qApp->applicationFilePath()) + " -hide");
    settings.sync();
}
//------------------------------------------------------------------------------
void AppCore::disableLaunchOnStartup()
{
#ifdef Q_OS_LINUX
    disableLaunchOnSturtupLinux();
#endif //Q_OS_LINUX

#ifdef Q_OS_WIN
    disableLaunchOnStartupWindows();
#endif //Q_OS_WIN
}
//------------------------------------------------------------------------------
void AppCore::disableLaunchOnSturtupLinux()
{
    QString autostartPath = QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).at(0) + QLatin1String("/autostart");
    QDir autorunDir(autostartPath);

    if(!autorunDir.exists()){
        autorunDir.mkpath(autostartPath);
    }

    QFile autorunFile(autostartPath + QLatin1String("/WallOne.desktop"));
    autorunFile.remove();
}
//------------------------------------------------------------------------------
void AppCore::disableLaunchOnStartupWindows()
{
    QSettings settings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);
    settings.remove(qApp->applicationName());
}
//------------------------------------------------------------------------------
void AppCore::parseAlbumList(QDir dir)
{
    QTimer t;
    t.singleShot(200, [this, dir]() {
        emit albumListDownloaded(parseFileList(dir));
    });
}
//------------------------------------------------------------------------------
QStringList AppCore::parseFileList(QDir dir)
{
    QStringList lines;
    QFile listFile(dir.path());

    if (!listFile.open(QIODevice::ReadOnly)) {
        return lines;
    }

    QTextStream fin(&listFile);

    while (!fin.atEnd()) {
        QString line = fin.readLine();
        lines.append(line);
    }

    return lines;
}
