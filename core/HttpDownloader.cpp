#include "HttpDownloader.hpp"

HttpDownloader::HttpDownloader(QObject *parent) : QObject(parent)
{
}

void HttpDownloader::download(QUrl url, QDir path)
{
    filePath = path;

    auto networkManager = new QNetworkAccessManager(this);
    connect(networkManager, &QNetworkAccessManager::finished,
            this, &HttpDownloader::replyFinished);

    QNetworkRequest networkRequest(url);
    auto networkReply = networkManager->get(networkRequest);
    Q_UNUSED(networkReply);
}

void HttpDownloader::replyFinished(QNetworkReply *networkReply)
{
    QFile file(filePath.path() + "/" + networkReply->url().fileName());
    if (file.open(QIODevice::WriteOnly)) {
        file.write(networkReply->readAll());        
        emit fileDownloaded(QDir(file.fileName()));
    }

    networkReply->deleteLater();
}
