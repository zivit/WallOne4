#pragma once

#include <QObject>
#include <QApplication>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFile>
#include <QDir>
#include <QUrl>

class HttpDownloader : public QObject
{
    Q_OBJECT
public:
    HttpDownloader(QObject *parent=0);

    void download(QUrl url, QDir path);
    void replyFinished(QNetworkReply *networkReply);

private:
    QDir filePath;

signals:
    void fileDownloaded(QDir);
};
