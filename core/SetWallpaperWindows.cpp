#include "SetWallpaperCross.hpp"
#include <QDir>
#include <QString>
#include <QSettings>
#include <QDebug>

#include "windows.h"

void setWallpaperCross(const QDir &filePath)
{
    QString path = QDir::currentPath();
    path = path.left(2) + filePath.path();
    qDebug() << "set wall:" << path;
    QSettings settings;
    settings.setValue("wallpaperPath", path);
    SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, (void*)(path.toStdWString().c_str()), SPIF_UPDATEINIFILE | SPIF_SENDCHANGE);
}
