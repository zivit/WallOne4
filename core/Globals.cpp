#include "Globals.hpp"

const QString Globals::WALLONE_VERSION = "0.2.0-alpha";
const QString Globals::WALLONE_VERSION_URL = "http://zivit.zzz.com.ua/wallone/VERSION";
const QString Globals::WALLONE_UPDATE_URL = "http://zivit.zzz.com.ua/wallone/updates/WallOne";
const QString Globals::WALLONE_DOMAIN = "http://zivit.zzz.com.ua";

const QString Globals::LIST_DIR = "/wallone/albums";
const QString Globals::UPDATES_DIR = "/wallone/updates";
