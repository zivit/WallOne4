#pragma once

#include <QObject>
#include <QSystemTrayIcon>
#include <QTimer>
#include <QMenu>
#include <QDir>
#include <QStandardPaths>
#include <QAction>
#include <QSettings>
#include <QProcess>

#include "core/Globals.hpp"
#include "core/HttpDownloader.hpp"
#include "core/WallpaperManager.hpp"

class AppCore : public QObject
{
    Q_OBJECT
public:
    explicit AppCore(QObject *parent = 0);

    int getWallpaperIndex(QDir dir);

signals:
    void albumListDownloaded(QStringList albumStringList);
    void wallpaperDownloaded(QUrl path);
    void trayShowTriggered();
    void trayMinimizeTriggered();
    void trayQuitTriggered();
    void hideWindow();
    void haveUpdate();

public slots:
    void updateAlbumList();
    void checkUpdates();
    void updateProgram();
    void downloadWallpaper(QUrl url);
    void setWallpapersCount(int count);
    void setWallpaper(QUrl url);
    QPointF cursorPos();
    void enableLaunchOnStartup();
    void disableLaunchOnStartup();

private slots:
    void saveVersionFile(QDir dir);
    void downloadUpdate();

private:
    int wallpapersCount;
    QSystemTrayIcon *trayIcon;

    void parseAlbumList(QDir);
    QStringList parseFileList(QDir dir);
    void enableLaunchOnStartupLinux();
    void enableLaunchOnStartupWindows();
    void disableLaunchOnSturtupLinux();
    void disableLaunchOnStartupWindows();
};
