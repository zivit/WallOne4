#pragma once

#include <QObject>
#include <QDir>

#include "SetWallpaperCross.hpp"

class WallpaperManager : public QObject
{
    Q_OBJECT
public:
    explicit WallpaperManager(QObject *parent = 0);
    void setWallpaper(const QDir &filePath);
};
