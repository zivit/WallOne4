#include "SetWallpaperCross.hpp"
#include <QDir>
#include <QString>
#include <QProcess>

void setWallpaperCross(const QDir &filePath)
{
    QString desktopEnvironment = std::getenv("XDG_CURRENT_DESKTOP");
    QString command;

    if ((desktopEnvironment == "Unity") || (desktopEnvironment == "Gnome")) {
        command = "gsettings set org.gnome.desktop.background picture-uri "
                  "\"file://" + filePath.path() + "\"";
    }

    if (desktopEnvironment == "KDE") {
        command = "dcop kdesktop KBackgroundIface setWallpaper "
                  "file://" + filePath.path() + " 1";
    }

    if (desktopEnvironment == "xfce") {
        command = "xfconf-query -c xfce4-desktop -p last-image -s "
                  "\"file://" + filePath.path() + "\"";
    }

    if (desktopEnvironment == "lxde") {
        command = "pcmanfm -w "
                  "\"" + filePath.path() + "\"";
    }

    if (desktopEnvironment == "Mate") {
        command = "gsettings set org.mate.background picture-filename "
                  "\"file://" + filePath.path() + "\"";
    }

    // TODO: Command for Cinnamon, Sugar...

    QProcess process;
    process.startDetached(command);
}
