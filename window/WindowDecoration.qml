import QtQuick 2.5
import QtQuick.Controls 1.4

ApplicationWindow {
    id: applicationWindow
    visible: true
    width: 488
    height: 344
    color: "#00000000"
    title: qsTr("WallOne")
    maximumWidth: width
    maximumHeight: height
    minimumWidth: width
    minimumHeight: height
    flags: Qt.FramelessWindowHint | Qt.WindowMinimizeButtonHint | Qt.WindowCloseButtonHint

    MainWindow {
        id: mainWindow
        x: 4
        y: 4
    }

    Image {
        id: topShadow
        height: 4
        anchors.rightMargin: 0
        anchors.leftMargin: 0
        fillMode: Image.Tile
        anchors.right: topRightShadow.left
        anchors.left: topLeftShadow.right
        anchors.top: parent.top
        source: "qrc:/images/resources/shadow/shadow_top.png"
    }

    Image {
        id: topLeftShadow
        width: 4
        height: 4
        visible: true
        fillMode: Image.Tile
        anchors.left: parent.left
        anchors.top: parent.top
        source: "qrc:/images/resources/shadow/shadow_top_left.png"
    }

    Image {
        id: topRightShadow
        width: 4
        height: 4
        visible: true
        fillMode: Image.Tile
        anchors.top: parent.top
        anchors.right: parent.right
        source: "qrc:/images/resources/shadow/shadow_top_right.png"
    }

    Image {
        id: leftShadow
        width: 4
        anchors.bottomMargin: 0
        anchors.topMargin: 0
        fillMode: Image.Tile
        anchors.top: topLeftShadow.bottom
        anchors.bottom: bottomLeftShadow.top
        anchors.left: parent.left
        source: "qrc:/images/resources/shadow/shadow_left.png"
    }

    Image {
        id: rightShadow
        width: 4
        anchors.bottomMargin: 0
        anchors.topMargin: 0
        fillMode: Image.Tile
        anchors.top: topRightShadow.bottom
        anchors.bottom: bottomRightShadow.top
        anchors.right: parent.right
        source: "qrc:/images/resources/shadow/shadow_right.png"
    }

    Image {
        id: bottomShadow
        height: 4
        anchors.rightMargin: 0
        anchors.leftMargin: 0
        fillMode: Image.Tile
        anchors.right: bottomRightShadow.left
        anchors.left: bottomLeftShadow.right
        anchors.bottom: parent.bottom
        source: "qrc:/images/resources/shadow/shadow_bottom.png"
    }

    Image {
        id: bottomLeftShadow
        width: 4
        height: 4
        visible: true
        fillMode: Image.Tile
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        source: "qrc:/images/resources/shadow/shadow_bottom_left.png"
    }

    Image {
        id: bottomRightShadow
        width: 4
        height: 4
        visible: true
        fillMode: Image.Tile
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        source: "qrc:/images/resources/shadow/shadow_bottom_right.png"
    }
}
