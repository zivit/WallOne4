import QtQuick 2.5
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.0
import "../ui/style"
import "../ui"

Item {
    id: mainWindow
    width: 480
    height: 336
    clip: true

    property bool canUpdate: false

    Settings {
        id: mainSettings
        property string currentImageSource
        property alias isPlaying: pauseButton.isPlaying
        property alias albumName: headerBar.currentAlbumName
        property date savedDate
        property int wallpapersCount
        property string wallpaperPath
        property bool isFirstRunning: true
    }

    Component.onCompleted: {
        if (mainSettings.isFirstRunning) {
            mainSettings.isFirstRunning = false;
            tresh.threshold = 0;
            headerBar.currentAlbumName = "Default";
            appCore.setWallpapersCount(1);
            appCore.downloadWallpaper(GLOBALS.WALLONE_DOMAIN + "/wallone/albums/cars");
            mainSettings.savedDate = new Date;
            appCore.enableLaunchOnStartup();
            appCore.checkUpdates();
        }

        currentImage.source = mainSettings.currentImageSource;
        console.log(mainSettings.currentImageSource);
    }

    Connections {
        target: appCore

        onWallpaperDownloaded: {
            currentImage.source = "file://" + path;
            mainSettings.currentImageSource = "file://" + path;
            if (pauseButton.isPlaying) {
                appCore.setWallpaper(currentImage.source);
            }
        }

        onTrayShowTriggered: {
            applicationWindow.show();
        }

        onTrayMinimizeTriggered: {
            applicationWindow.hide();
        }

        onTrayQuitTriggered: {
            if (canUpdate) {
                appCore.updateProgram();
            }

            Qt.quit();
        }

        onHideWindow: {
            applicationWindow.hide();
        }

        onHaveUpdate: {
            canUpdate = true;

            if (!applicationWindow.visible) {
                appCore.updateProgram();
            }
        }
    }

    HeaderBar {
        id: headerBar
    }

    Rectangle {
        color: "#59A1C7"
        anchors.fill: currentImage
    }

    Image {
        id: currentImage
        clip: true
        smooth: true
        fillMode: Image.PreserveAspectCrop
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.top: headerBar.bottom
        horizontalAlignment: Qt.AlignLeft
        verticalAlignment: Qt.AlignVCenter

        onSourceChanged: {
            tresh.anim.running = true;
        }
    }

    Image {
        id: currentImageGrey
        clip: true
        smooth: true
        width: 480
        fillMode: Image.PreserveAspectCrop
        anchors.left: currentImage.left
        anchors.top: currentImage.top
        anchors.bottom: currentImage.bottom
        source: currentImage.source
        horizontalAlignment: Qt.AlignLeft
        verticalAlignment: Qt.AlignVCenter
        visible: pauseButton.isPlaying

        Desaturate {
            anchors.fill: currentImageGrey
            source: currentImageGrey
            desaturation: 1.0
        }
    }

    Image {
        id: mask
        source: "qrc:/images/resources/fog.png"
        sourceSize: Qt.size(currentImageForeground.width, currentImageForeground.height)
        smooth: true
        visible: false
    }

    Image {
        id: currentImageForeground
        clip: true
        smooth: true
        anchors.leftMargin: 0
        fillMode: Image.PreserveAspectCrop
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: headerBar.bottom
        anchors.bottom: parent.bottom
        horizontalAlignment: Qt.AlignLeft
        visible: false

        Component.onCompleted: {
            source = currentImage.source;
        }
    }

    ThresholdMask {
        id: tresh
        anchors.fill: currentImageForeground
        source: currentImageForeground
        maskSource: mask
        threshold: 1
        spread: 0.9

        property alias anim: treshAnim

        PropertyAnimation {
            id: treshAnim
            target: tresh
            property: "threshold"
            duration: 2000
            from: 0
            to: 1

            onRunningChanged: {
                if (running == false) currentImageForeground.source = currentImage.source;
            }
        }
    }

    Timer {
        interval: 60000
        running: true
        repeat: true
        triggeredOnStart: true

        property int updateCheckCounter: 0

        onTriggered: {
            var date = new Date;
            if (pauseButton.isPlaying && date.getDay() !== mainSettings.savedDate.getDay()) {
                tresh.threshold = 0;
                appCore.downloadWallpaper(mainSettings.wallpaperPath);
                mainSettings.savedDate = new Date
            }

            ++updateCheckCounter;
            if (updateCheckCounter >= 10) {
                updateCheckCounter = 0;
                appCore.checkUpdates();
            }

            if (canUpdate) {
                if (!applicationWindow.visible) {
                    appCore.updateProgram();
                }
            }

            var nowSeconds = date.getHours() * 60 + date.getMinutes();
            currentImageGrey.width = 480 * (nowSeconds / 1440);
        }
    }

    Button {
        id: pauseButton
        width: 86
        height: 86
        anchors.verticalCenter: currentImage.verticalCenter
        anchors.horizontalCenter: currentImage.horizontalCenter
        iconSource: "qrc:/images/resources/play.png"
        style: PauseButtonStyle {}

        property bool isPlaying: false

        onClicked: {
            if (isPlaying === true) {
                pauseButton.iconSource = "qrc:/images/resources/play.png";
                pauseButton.isPlaying = false;
            } else {
                pauseButton.iconSource = "qrc:/images/resources/pause.png";
                pauseButton.isPlaying = true;
                appCore.setWallpaper(currentImage.source);
            }
        }

        Component.onCompleted: {
            if (isPlaying === true) {
                pauseButton.iconSource = "qrc:/images/resources/pause.png";
                pauseButton.isPlaying = true;
            } else {
                pauseButton.iconSource = "qrc:/images/resources/play.png";
                pauseButton.isPlaying = false;
            }
        }
    }

    Button {
        id: infoButton
        visible: false
        width: 36
        height: 36
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 12
        iconSource: "qrc:/images/resources/info.png"
        style: InfoButtonStyle {}

        onClicked: {
            if (infoMenu.state === "showed") {
                infoMenu.state = "hidden"
            } else {
                infoMenu.state = "showed"
            }
        }
    }

    GamburgerMenu {
        id: gamburgerMenu
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: headerBar.bottom
        anchors.topMargin: 0
    }

    InfoMenu {
        id: infoMenu
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: headerBar.bottom
        anchors.topMargin: 0
    }

    AlbumsMenu {
        id: albumsMenu
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: headerBar.bottom
        anchors.topMargin: 0
    }

    Rectangle {
        id: shadowBox
        height: 3
        visible: false
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#33000000"
            }

            GradientStop {
                position: 1
                color: "#00000000"
            }
        }
        z: 5
        anchors.right: headerBar.right
        anchors.left: headerBar.left
        anchors.top: headerBar.bottom
    }
}
