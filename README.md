# WallOne

Wallpaper for one day.

## Running

The program using Qt5 and running on Linux (tested on Ubuntu 16.04 x64) and Windows (tested on 10). Mac OS will make in future.

## Screenshots
![screenshot](screenshot1.png)
![screenshot](screenshot2.png)
![screenshot](screenshot3.png)

## Authors

* **Vitaliy Zinchenko** - *Developer*
* **Bogdan Slomchinskiy** - *Designer*

## License

This project is licensed under the Commercial License - see the [LICENSE.md](LICENSE.md) file for details
