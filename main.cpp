#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "core/Globals.hpp"
#include "core/AppCore.hpp"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setOrganizationName("PopaBanana");
    app.setOrganizationDomain("popabanana.com");
    app.setApplicationName("WallOne");
    app.setWindowIcon(QIcon(":/images/resources/wallone.ico"));

    QQmlApplicationEngine engine;

    auto context = engine.rootContext();
    auto appCore = new AppCore();
    context->setContextProperty("appCore", appCore);
    auto globals = new Globals();
    context->setContextProperty("GLOBALS", globals);

    engine.load(QUrl(QStringLiteral("qrc:/window/WindowDecoration.qml")));

    for (int i = 1; i < argc; ++i) {
        if (qstrcmp(argv[i], "-hide") == 0) {
            QApplication::processEvents();
            appCore->hideWindow();
        }
    }

    return app.exec();
}
